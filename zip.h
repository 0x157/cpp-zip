#ifndef _MB_ZIP_H_
#define _MB_ZIP_H_

#include <tuple>
#include <type_traits>
#include <stdexcept>

namespace mb
{

template <typename... Ts>
struct Zipper : public std::tuple<Ts...>
{
    template <typename U>
    struct get_iterator_type
    {
        using type = typename U::iterator;
    };

    template <typename U>
    struct get_iterator_type<const U>
    {
        using type = typename U::const_iterator;
    };

    template <typename U>
    struct get_value_reference_type
    {
        using type = std::remove_reference_t<typename U::value_type>&;
    };

    template <typename U>
    struct get_value_reference_type<const U>
    {
        using type = const std::remove_reference_t<typename U::value_type>&;
    };

    template <typename... Us>
    using iter_tuple_type =
        std::tuple<typename get_iterator_type<std::remove_reference_t<Us>>::type...>;

    template <typename... Us>
    using ref_tuple_type =
        std::tuple<typename get_value_reference_type<std::remove_reference_t<Us>>::type&...>;

    template <typename... Us>
    struct Iterator
    {
    private:
        using ref_tuple = ref_tuple_type<Us...>;
        using iter_tuple = iter_tuple_type<Us...>;

    public:

        constexpr ref_tuple operator*()
        {
            return TupleHelper<sizeof...(Us)>::toTuple(iters);
        }

        constexpr ref_tuple operator->()
        {
            return *(*this);
        }

        constexpr bool operator==(const Iterator<Us...> &other) const
        {
            return iters == other.iters;
        }

        constexpr bool operator!=(const Iterator<Us...> &other) const
        {
            return !(*this==other);
        }

        constexpr Iterator<Us...>& operator++()
        {
            TupleHelper<sizeof...(Us)>::inc(iters);
            return *this;
        }

        constexpr Iterator<Us...> operator++(int)
        {
            auto copy = *this;
            TupleHelper<sizeof...(Us)>::inc(iters);
            return copy;
        }

        iter_tuple iters;

    private:
        template <std::size_t count, std::size_t... ns>
        struct TupleHelper
        {
            constexpr static ref_tuple toTuple(iter_tuple &iterTuple)
            {
                return TupleHelper<count-1, count-1, ns...>::toTuple(iterTuple);
            }

            constexpr static void inc(iter_tuple &iterTuple)
            {
                ++std::get<count-1>(iterTuple);
                TupleHelper<count-1, count-1, ns...>::inc(iterTuple);
            }
        };

        template <std::size_t... ns>
        struct TupleHelper<0, ns...>
        {
            constexpr static ref_tuple toTuple(iter_tuple &iterTuple)
            {
                return ref_tuple(*std::get<ns>(iterTuple)...);
            }

            constexpr static void inc(iter_tuple&)
            {}
        };
    };

    template <std::size_t pos, typename... Us>
    struct Helper
    {
    private:
        using iter_tuple = iter_tuple_type<Us...>;
        using const_iter_tuple = iter_tuple_type<std::remove_reference_t<Us> const...>;
        using container_tuple = std::tuple<Us...>;

    public:

        constexpr static void fillbegin(const container_tuple &containers, iter_tuple &iters)
        {
            std::get<pos>(iters) = std::get<pos>(containers).begin();
            Helper<pos-1, Us...>::fillbegin(containers, iters);
        }

        constexpr static void cfillbegin(const container_tuple &containers, const_iter_tuple &iters)
        {
            std::get<pos>(iters) = std::get<pos>(containers).cbegin();
            Helper<pos-1, Us...>::cfillbegin(containers, iters);
        }

        constexpr static void fillend(const container_tuple &containers, iter_tuple &iters)
        {
            std::get<pos>(iters) = std::get<pos>(containers).end();
            Helper<pos-1, Us...>::fillend(containers, iters);
        }

        constexpr static void cfillend(const container_tuple &containers, const_iter_tuple &iters)
        {
            std::get<pos>(iters) = std::get<pos>(containers).cend();
            Helper<pos-1, Us...>::cfillend(containers, iters);
        }

        constexpr static void checkBalanced(const container_tuple &containers)
        {
            Helper<pos-1, Us...>::checkBalanced(containers, std::get<pos>(containers).size());
        }

        constexpr static void checkBalanced(
                const container_tuple &containers, std::size_t expectedSize)
        {
            if (std::get<pos>(containers).size() != expectedSize)
                throw std::length_error("zip elemnts with different lengths");

            Helper<pos-1, Us...>::checkBalanced(containers, expectedSize);
        }
    };

    template <typename... Us>
    struct Helper <0, Us...>
    {
    private:
        using iter_tuple = iter_tuple_type<Us...>;
        using container_tuple = std::tuple<Us...>;
        using const_iter_tuple = iter_tuple_type<std::remove_reference_t<Us> const...>;

    public:

        constexpr static void fillbegin(const container_tuple &containers, iter_tuple &iters)
        {
            std::get<0>(iters) = std::get<0>(containers).begin();
        }

        constexpr static void cfillbegin(const container_tuple &containers, const_iter_tuple &iters)
        {
            std::get<0>(iters) = std::get<0>(containers).cbegin();
        }

        constexpr static void fillend(const container_tuple &containers, iter_tuple &iters)
        {
            std::get<0>(iters) = std::get<0>(containers).end();
        }

        constexpr static void cfillend(const container_tuple &containers, const_iter_tuple &iters)
        {
            std::get<0>(iters) = std::get<0>(containers).cend();
        }

        constexpr static void checkBalanced(const container_tuple &)
        {}

        constexpr static void checkBalanced(
                const container_tuple &containers, std::size_t expectedSize)
        {
            if (std::get<0>(containers).size() != expectedSize)
                throw std::length_error("zip elemnts with different lengths");
        }
    };

public:

    using iterator = Iterator<Ts...>;

    // references "can't" be cv-qualified, see c++14 8.3.2
    using const_iterator = Iterator<std::remove_reference_t<Ts> const...>;

    template <typename... Us>
    constexpr Zipper(Us&&... args)
    : std::tuple<Ts...>(std::forward<Us>(args)...)
    {
        static_assert(sizeof...(Ts)>0, "No containers provided");
    }

    constexpr iterator begin()
    {
        Helper<sizeof...(Ts)-1, Ts...>::checkBalanced(*this);

        iterator iter;
        Helper<sizeof...(Ts)-1, Ts...>::fillbegin(*this, iter.iters);
        return iter;
    }

    constexpr const_iterator begin() const
    {
        Helper<sizeof...(Ts)-1, Ts...>::checkBalanced(*this);

        const_iterator iter;
        Helper<sizeof...(Ts)-1, Ts...>::cfillbegin(*this, iter.iters);
        return iter;
    }

    constexpr iterator end()
    {
        iterator iter;
        Helper<sizeof...(Ts)-1, Ts...>::fillend(*this, iter.iters);
        return iter;
    }

    constexpr const_iterator end() const
    {
        const_iterator iter;
        Helper<sizeof...(Ts)-1, Ts...>::cfillend(*this, iter.iters);
        return iter;
    }
};


template <typename... Ts>
constexpr typename Zipper<Ts...>::iterator begin(Zipper<Ts...> &zip)
{
    return zip.begin();
}

template <typename... Ts>
constexpr typename Zipper<Ts...>::const_iterator begin(const Zipper<Ts...> &zip)
{
    return zip.begin();
}

template <typename... Ts>
constexpr typename Zipper<Ts...>::iterator end(Zipper<Ts...> &zip)
{
    return zip.end();
}

template <typename... Ts>
constexpr typename Zipper<Ts...>::const_iterator end(const Zipper<Ts...> &zip)
{
    return zip.end();
}

template <typename T>
T& getContainerType(T&);

template <typename T>
T getContainerType(T&&);

template <typename... Ts>
constexpr auto zip(Ts&&... args)
{
    return Zipper<decltype(getContainerType(std::forward<Ts>(args)))...>(std::forward<Ts>(args)...);
}


} // ns mb

#endif
