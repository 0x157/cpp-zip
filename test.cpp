#if __cplusplus >= 201703L // https://gcc.gnu.org/onlinedocs/cpp/Standard-Predefined-Macros.html
#define USECPP17
#endif

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h> // https://github.com/onqtam/doctest

#include "zip.h"

#include <list>
#include <type_traits>
#include <vector>
#include <array>


TEST_CASE("getContainerType")
{
    int a = 0;
    static_assert(std::is_same<int&, decltype(mb::getContainerType(a))>::value, "");
    static_assert(std::is_same<int, decltype(mb::getContainerType(std::move(a)))>::value, "");

    const int ca = 0;
    static_assert(std::is_same<const int&, decltype(mb::getContainerType(ca))>::value, "");
    // getContainerType with rvalue parameter returns a prvalue. If such a return type
    // is not of class or array type, then it can't be cv-qualified. E.g. see c++14 section 3.10 .
    static_assert(std::is_same<int, decltype(mb::getContainerType(std::move(ca)))>::value, "");

    const std::vector<int> cv;
    static_assert(
            std::is_same<const std::vector<int>&, decltype(mb::getContainerType(cv))>::value,
            "");
    static_assert(
            std::is_same<
                const std::vector<int>,
                decltype(mb::getContainerType(std::move(cv)))>::value,
            "");
}

TEST_CASE("zip - return types")
{
    std::vector<int> a;
    static_assert(
            std::is_same<
                mb::Zipper<std::vector<int>&>,
                decltype(mb::getContainerType(mb::zip(a)))>::value
            , "lvalue reference");
    static_assert(
            std::is_same<
                mb::Zipper<std::vector<int>>,
                decltype(mb::getContainerType(mb::zip(std::move(a))))>::value
            , "rvalue reference");

    const std::vector<int> ca;
    static_assert(
            std::is_same<
                mb::Zipper<const std::vector<int>&>,
                decltype(mb::getContainerType(mb::zip(ca)))>::value
            , "const lvalue reference");
    static_assert(
            std::is_same<
                mb::Zipper<const std::vector<int>>,
                decltype(mb::getContainerType(mb::zip(std::move(ca))))>::value
            , "const rvalue reference");

    static_assert(
            std::is_same<
                mb::Zipper<std::vector<int>>,
                decltype(mb::getContainerType(mb::zip(std::vector<int>{})))>::value
            , "rvalue reference - temporary");
}

TEST_CASE("Zipper with two containers")
{
    std::vector<int> a{{2, 2}};
    std::list<double> b{{3., 3.}};
    mb::Zipper<std::vector<int>&, std::list<double>&> z(a, b);

    unsigned count = 0;
    for (auto it=z.begin(); it!=z.end(); ++it)
    {
        ++count;
    }

    CHECK(count == 2u);
}

TEST_CASE("Zipper increment")
{
    std::vector<int> a{{2, 3}};
    auto z = mb::zip(a);

    auto it = z.begin();
    auto it2 = z.begin();

    CHECK(it == it2);

    auto it_old = it++;
    CHECK(it_old != it);
    CHECK(it_old == it2);

    auto it_new = ++it2;
    CHECK(it_new == it);
    CHECK(it2 == it);
}

struct ConstChecker
{
    bool isConst() const { return true; }
    bool isConst() { return false; }
};

TEST_CASE("Zipper const container")
{
    std::vector<ConstChecker> a({ConstChecker{}});
    mb::Zipper<const std::vector<ConstChecker>&, std::vector<ConstChecker>&> z(a, a);
    auto it = z.begin();
    CHECK(it != z.end());

    CHECK(std::get<0>(*it).isConst() == true);
    CHECK(std::get<1>(*it).isConst() == false);
}

TEST_CASE("iterate zip")
{
    std::vector<int> a{{1, 2, 3}};
    int count = 0;
    for (auto t : mb::zip(a))
    {
        CHECK(std::get<0>(t) == ++count);
    }
    CHECK(count == 3);
}

TEST_CASE("iterate const zip")
{
    std::vector<int> a{{1, 2, 3}};
    const auto z = mb::zip(a);
    int count = 0;
    for (auto t : z)
    {
        static_assert(std::is_same<const int&, decltype(std::get<0>(t))>::value, "");
        CHECK(std::get<0>(t) == ++count);
    }
    CHECK(count == 3);
}

TEST_CASE("zip adjust elements")
{
    std::vector<int> a{{1, 2}};
    std::vector<int> b{{2, 1}};

    int count = 0;
    for (auto p : mb::zip(a, b))
    {
        auto &ae = std::get<0>(p);
        auto &be = std::get<1>(p);

        CHECK(ae == count+1);
        CHECK(be == 2-count);
        ++ae;
        --be;
        ++count;
    }
    CHECK(count == 2);

    count = 0;
    for (auto p : mb::zip(a, b))
    {
        auto &ae = std::get<0>(p);
        auto &be = std::get<1>(p);

        CHECK(ae == count+2);
        CHECK(be == 1-count);
        ++count;
    }
}

TEST_CASE("zip containers with different sizes")
{
    std::vector<int> a{{1, 2}};
    std::vector<int> b{{2, 1, 0}};
    std::vector<int> c{{2, 1, 0}};

    auto z = mb::zip(a, b, c);
    CHECK_THROWS(z.begin());

    a.push_back(3);
    CHECK_NOTHROW(z.begin());

    c.push_back(4);
    CHECK_THROWS(z.begin());
}

TEST_CASE("zip - test tuple like behavior")
{
    std::vector<int> a{{1, 2}};
    std::vector<int> b{{3, 2}};

    auto z = mb::zip(a, b);
    auto &stored_a = std::get<0>(z);
    auto &stored_b = std::get<1>(z);

    CHECK(&a == &stored_a);
    CHECK(&b == &stored_b);
}

struct DummyContainer : std::vector<int>
{
    enum class CreationType
    {
        DEFAULT,
        COPY,
        MOVE
    };

    DummyContainer()
    : std::vector<int>{}
    , type(CreationType::DEFAULT)
    {}

    DummyContainer(const DummyContainer &a)
    : std::vector<int>{a}
    , type(CreationType::COPY)
    {}

    DummyContainer(DummyContainer &&a)
    : std::vector<int>{std::move(a)}
    , type(CreationType::MOVE)
    {}

    CreationType type;
};

TEST_CASE("zip r-reference")
{
    DummyContainer a;
    CHECK(a.type == DummyContainer::CreationType::DEFAULT);
    DummyContainer b;
    CHECK(b.type == DummyContainer::CreationType::DEFAULT);

    auto z = mb::zip(a, std::move(b));
    CHECK(&std::get<0>(z) == &a);
    CHECK(std::get<0>(z).type == DummyContainer::CreationType::DEFAULT);
    CHECK(&std::get<1>(z) != &b);
    CHECK(std::get<1>(z).type == DummyContainer::CreationType::MOVE);
}

TEST_CASE("user zipper-object repeatedly")
{
    std::vector<int> a{{2, 2}};
    std::list<double> b{{3., 3.}};
    auto z = mb::zip(a, b);

    for (unsigned i=0; i<2; ++i)
    {
        unsigned count = 0;
        for (auto it=z.begin(); it!=z.end(); ++it)
        {
            ++count;
        }

        CHECK_MESSAGE(count == 2u, "iteration " << i);
    }
}

#ifdef USECPP17
TEST_CASE("zip adjust elements - structured bindings")
{
    std::vector<int> a{{1, 2}};
    std::vector<int> b{{2, 1}};

    int count = 0;
    for (auto [ae, be] : mb::zip(a, b))
    {
        CHECK(ae == count+1);
        CHECK(be == 2-count);
        ++ae;
        --be;
        ++count;
    }
    CHECK(count == 2);

    count = 0;
    for (auto [ae, be] : mb::zip(a, b))
    {
        CHECK(ae == count+2);
        CHECK(be == 1-count);
        ++count;
    }
}

TEST_CASE("zip test conexpr")
{
    auto crossproduct = []()
    {
        constexpr std::array<int, 2> a{{1, 2}};
        constexpr std::array<int, 2> b{{2, 1}};
        int result = 0;
        for (auto [ae, be] : mb::zip(a, b))
        {
            result += ae*be;
        }
        return result;
    };

    constexpr auto result = crossproduct();
    static_assert(result == 4);

    CHECK(result == 4);
}

TEST_CASE("zip test conexpr - mutable container")
{
    auto crossproduct = []()
    {
        std::array<int, 2> a{{0, 0}};
        std::array<int, 2> b{{0, 0}};
        for (auto [ae, be] : mb::zip(a, b))
        {
            ae += 3;
            be += 2;
        }

        int result = 0;
        for (auto [ae, be] : mb::zip(a, b))
        {
            result += ae*be;
        }
        return result;
    };

    constexpr auto result = crossproduct();
    static_assert(result == 12);

    CHECK(result == 12);
}
#endif // c++17 tests
