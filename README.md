This is a simple header only library for c++ that provides a python like zip functionality.
This allows you to iterate multiple containers simultaneously.

The minimal required c++ version is c++14.

# Introduction
The helper function mb::zip(c1, c2, ...) returns an object of type mb::Zipper<C1, C2, ...>
which then can be used for iteration. Iterations can be done using begin()/end() or
range-based for loops.
The iterators points to a std::tuple that refrences the corresponding elements of the containers.

Example 1:
```c++
std::vector<int> v1;
std::vector<double> v2;
...
for (auto p : mb::zip(v1, v2))
{
    auto &a = std::get<0>(p);
    ...
}
```

Example 2 - c++17 and structures bindings:
```c++
std::vector<int> v1;
std::vector<double> v2;
...
for (auto [a, b] : mb::zip(v1, v2))
{
    ++a;
    ++b;
    ...
}
```

# Build test
-  Ensure that you have doctest (https://github.com/onqtam/doctest).
cmake should take care of that, if not you can do this manually:  
```git submodule update --init --recursive```
- ```mkdir build```
- ```cd build```
- ```cmake -G "Unix Makefiles" ..```
- ```make```

To create compile commands (e.g. for YouCompleteMe)
- ```cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -G "Unix Makefiles" ..; cp compile_commands.json ..```
